Pod::Spec.new do |s|
  s.name             = "SinfoUtils"
  s.version          = "1.1.0"
  s.summary          = "Framework da Sinfo para utilizar métodos comuns entre diversas aplicações"
  s.homepage         = "http://www.info.ufrn.br"
  s.author           = { "Superintendência de Informática" => "mobile@info.ufrn.br" }
  s.source           = { :git => "git@gitdesenvolvimento.info.ufrn.br:mobile/sinfo-utils-ios.git", :tag => s.version.to_s }
  s.platform     = :ios, '8.0'
  s.requires_arc = true
  s.source_files = 'SinfoUtils/*.{swift,h,m}'
  s.frameworks = 'UIKit'
  s.module_name = 'SinfoUtils'
end