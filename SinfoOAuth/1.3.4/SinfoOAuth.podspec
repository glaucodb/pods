Pod::Spec.new do |s|
  s.name             = "SinfoOAuth"
  s.version          = "1.3.4"
  s.summary          = "Framework da Sinfo para se conectar ao OAuth na plataforma iOS."
  s.homepage         = "http://www.info.ufrn.br"
  s.author           = { "Superintendência de Informática" => "mobile@info.ufrn.br" }
  s.source           = { :git => "git@gitdesenvolvimento.info.ufrn.br:mobile/SinfoOAuth.git", :tag => s.version.to_s }
  s.platform     = :ios, '8.0'
  s.requires_arc = true
  s.source_files = 'SinfoOAuth/*.{swift,h,m}'
  s.frameworks = 'UIKit'
  s.module_name = 'SinfoOAuth'
end