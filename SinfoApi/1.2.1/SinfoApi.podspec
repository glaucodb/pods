Pod::Spec.new do |s|
  s.name             = "SinfoApi"
  s.version          = "1.2.1"
  s.summary          = "Framework da Sinfo para se conectar a ApiUFRN na plataforma iOS."
  s.homepage         = "http://www.info.ufrn.br"
  s.author           = { "Superintendência de Informática" => "mobile@info.ufrn.br" }
  s.source           = { :git => "git@gitdesenvolvimento.info.ufrn.br:mobile/SinfoApi.git", :tag => s.version.to_s }
  s.platform     = :ios, '8.0'
  s.requires_arc = true
  s.source_files = 'SinfoApi/*.{swift,h,m}'
  s.frameworks = 'UIKit'
  s.module_name = 'SinfoApi'
end